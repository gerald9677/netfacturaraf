﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : DaoImplements, IDaoProducto
    {
        private const string FILENAME_HEADER = "hproducto.dat";
        private const string FILENAME_DATA = "dproducto.dat";
        private const int SIZE = 371;

        public DaoImplementsProducto() { }

        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            List<Producto> productos = new List<Producto>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string sku = brdata.ReadString();
                string nombre = brdata.ReadString();
                string descripcion = brdata.ReadString();
                int cantidad = brdata.ReadInt32();
                double precio = brdata.ReadDouble();
                Producto p = new Producto(id, sku, nombre,
                    descripcion, cantidad, precio);
                productos.Add(p);
            }

            close();
            return productos;
        }

        public Producto findById(int id)
        {
            Producto p = null;
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int ppos;
            for (ppos = 0; ppos < n; ppos++)
            {
                int pid = brheader.ReadInt32();
                if (pid == id)
                {
                    long dpos = ppos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string sku = brdata.ReadString();
                    string nombre = brdata.ReadString();
                    string descripcion = brdata.ReadString();
                    int cantidad = brdata.ReadInt32();
                    double precio = brdata.ReadDouble();
                    p = new Producto(id, sku, nombre,
                        descripcion, cantidad, precio);

                    break;
                }
            }
            close();

            return p;
        }

        public List<Producto> findByName(string name)
        {
            throw new NotImplementedException();
        }

        public Producto findBySku(string sku)
        {
            throw new NotImplementedException();
        }

        public void save(Producto t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(t.Sku);
            bwdata.Write(t.Nombre);
            bwdata.Write(t.Descripcion);
            bwdata.Write(t.Cantidad);
            bwdata.Write(t.Precio);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            close();
        }

        public int update(Producto t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(t.Id);
                    bwdata.Write(t.Sku);
                    bwdata.Write(t.Nombre);
                    bwdata.Write(t.Descripcion);
                    bwdata.Write(t.Cantidad);
                    bwdata.Write(t.Precio);
                    close();

                    return t.Id;
                }
            }
            return -1;
        }
    }
}
